/**
 * @file gui.cc
 * @author J.Ropars 364156 and E.Dutruy 355726
 * @date 2023-04-15
 * @version Rendu2
 * @brief Interface of Graphic User Interface (GUI) module
 */

#ifndef GUI_H
#define GUI_H

#include <gtkmm.h>
#include "graphic.h"
#include "simulation.h"
#include "constantes.h"


// --------------- Drawing Area ---------------
class DrawingArea : public Gtk::DrawingArea
{
private:
    Frame frame_;
	bool empty_;

    void adjust_frame(int width, int height);
    
protected:

    /**
     * @brief draw the modele on the screen
     * 
     * @param cr pointer
     * @param width 
     * @param height 
     */
	void on_draw(const Cairo::RefPtr<Cairo::Context>& cr, int width, int height);

public:
	DrawingArea();
	virtual ~DrawingArea();

    /**
     * @brief force gui to draw just the base and nothing else 
     * 
     */
    void clear();

	/**
	 * @brief actualise and call on_draw
	 */
	void draw();
};



// --------------- Main window ---------------
class MyWindow : public Gtk::Window 
{
private:
    unsigned int timer_val_;
    bool timer_added_; 
	bool disconnect_; 

    void set_button_label();
    void set_up_window();
    void connect_buttons();
    void connect_keyboard();
    void set_up_timer();
    void restart(std::string filename);
    void action_start();
    void action_step();
    void on_dialog_response(int response_id, Gtk::FileChooserDialog* dialog);
    bool on_key_pressed(guint keyval, guint keycode, Gdk::ModifierType state);
    bool on_timeout();

protected:
    // Drawing area
    DrawingArea m_drawing_area;

    // Signal handlers
    void exit_button_clicked();
    void open_button_clicked();
    void save_button_clicked();
    void start_button_clicked(); 
    void step_button_clicked();

    // Member widgets:
    Gtk::Box m_box_top, m_left_box, m_right_box, m_box_buttons, m_box_info;
    Gtk::Separator m_separator1;
    Gtk::Label m_label_general;
    Gtk::Label m_label_info;
    Gtk::Button m_exit_button;
    Gtk::Button m_open_button;
    Gtk::Button m_save_button;
    Gtk::Button m_start_button;
    Gtk::Button m_step_button;

public:
	MyWindow(Simulation* simptr);
    virtual ~MyWindow();
};

#endif /* GUI_H */