/**
 * @file simulation.h
 * @author J.Ropars 364156 and E.Dutruy 355726
 * @date 2023-03-07
 * @version Rendu2
 * @brief simulation module interface
 */

#ifndef SIMULATION_H
#define SIMULATION_H

#include <vector>
#include <random>
#include <iostream>
#include "particule.h"
#include "robot.h"

/**
 * @brief Simulation class is used to represent all the simulation context.
 */
class Simulation
{
private:
    std::string filename_;
    std::vector<double> raw_data_input_;
    int nbr_particules_;
    std::vector<Particule> particule_list_;
    std::vector<Reparateur> reparateur_list_;
    std::vector<Neutraliseur> neutraliseur_list_;
    bool simisok_;
    bool good_to_go;
    double proba_;

    Spatial space_robot_;
    
    void error_handler(std::string msg);
    void get_data_from_file(std::ifstream &file);
    void init_values(std::vector<double> &data_input);
    void read_file(std::string filename);
    void reset_all_values();
    std::string get_file_content();
    void desintegrate(int particule_pos);
    int get_nbNbreak() const;

public:
    Simulation();
    ~Simulation();

    /**
     * @brief initialize a new simulation
     * 
     * @return true if everything went well
     */
    bool init(std::string filename);

    /**
     * @brief go to the next update of the simulation
     * 
     */
    void update();

    /**
     * @brief draw the current state of simulation
     * 
     */
    void draw();

    /**
     * @brief write a text file indicating the current state of the simulation
     * 
     */
    void write_file();

    /**
     * @brief get a text containing all the info directly in the right format
     * 
     * @return std::string content
     */
    std::string sum_up_for_info_label();
};


#endif /* SIMULATION_H */
