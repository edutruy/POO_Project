/**
 * @file particule.h
 * @author J.Ropars 364156 and E.Dutruy 355726
 * @date 2023-03-10
 * @version Rendu2
 * @brief particule module interface
 */

#ifndef PARTICULE_H
#define PARTICULE_H

#include <vector>
#include "shape.h"


class Particule
{
private:
    shape::square shape_;

public:
    Particule(double x, double y, double size);

    shape::square get_shape() const;
    
    /**
     * @brief check if a particule is ok with the project directive
     * 
     * @param particule_list std::vector<Paricule>
     * @param ezero_on bool
     * @return true if it's allright
     */
    bool is_ok(std::vector<Particule> particule_list, bool ezero_on) const;

    /**
     * @brief get all the relevant informations in the format for the text file
     */
    std::string info();
    
    /**
     * @brief draw the particule
     * 
     */
    void draw();
    
    /**
     * @brief check if a particule can be desintegrate again
     * 
     * @return true if the particule is large enough
     */
    bool large_enough() const;

    /**
     * @brief 
     * 
     * @param indicator 4 options (0 top right, 1 top left, 2 bot right, 3 bot left)
     * @return One of the new particule after a desintegration
     */
    Particule sub_particule(int indicator) const;
};

#endif /* PARTICULE_H */
