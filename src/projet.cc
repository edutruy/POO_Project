/**
 * @file main.cc
 * @author J.Ropars 364156 (0%) and E.Dutruy 355726 (100%)
 * @date 2023-03-07
 * @version Rendu2
 * @brief main file of the project
 */

#include <iostream>
#include <gtkmm.h>
#include "simulation.h"
#include "gui.h"

int main(int argc, char *argv[])
{
    #ifdef DEBUG
    std::cout << "!!! DEBUG flag is define !!! \nPlease enter 'make clean' "
    << "and then 'make' if you want the standard version\n\n";
    #endif

    auto app = Gtk::Application::create("EPFL-POO-Project");
    
    Simulation* simptr(new Simulation);
    
    // check if there is an argument
    if (argc > 1)
    {
        simptr->init(argv[1]);
    }
    
	return app->make_window_and_run<MyWindow>(1, argv, simptr);
}