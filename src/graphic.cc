/**
 * @file graphics.cc
 * @author J.Ropars 364156 (90%) and E.Dutruy 355726 (10%)
 * @date 2023-03-24
 * @version Rendu2
 * @brief Pour le GUI avec GTKmm4 (implémentation)
 */

#include <iostream>
#include <cairomm/context.h>
#include "graphic.h"
#include "constantes.h"


static Cairo::RefPtr<Cairo::Context> cr; 
static const double linewid(0.5);

/**
 * @brief set the color in which we want to draw
 * 
 * @param colour 
 * @return true 
 * @return false 
 */
static bool set_color(Colour colour)
{
    bool check(true);
    switch (colour)
    {
    case NONE:
        check = false;
        break;
    case ROUGE:
        cr->set_source_rgb(1., 0., 0.);
        break;
    case GRIS:
        cr->set_source_rgb(0.66, 0.66, 0.66);
        break;
    case BLEU:
        cr->set_source_rgb(0., 0., 1.);
        break;
    case VERT:
        cr->set_source_rgb(0., 1., 0.);
        break;
    case VIOLET:
        cr->set_source_rgb(0.54, 0.17, 0.88);
        break;
    case ORANGE:
        cr->set_source_rgb(1., 0.64, 0.65);
        break;
    case BLACK:
        cr->set_source_rgb(0., 0., 0.);
        break;
    case BLANC:
        cr->set_source_rgb(1., 1., 1.);
        break;
    default:
        break;
    }
    return check;
}


void graphic::graphic_set_context(const Cairo::RefPtr<Cairo::Context>& ptcrr)
{
    cr = ptcrr;
}


void graphic::orthographic_projection(Frame frame)
{
	// déplace l'origine au centre de la fenêtre
	cr->translate(frame.width/2, frame.height/2);
  
	// normalise la largeur et hauteur aux valeurs fournies par le cadrage
	// ET inverse la direction de l'axe Y
	cr->scale(frame.width/(frame.xMax - frame.xMin), 
             -frame.height/(frame.yMax - frame.yMin));
  
	// décalage au centre du cadrage
	cr->translate(-(frame.xMin + frame.xMax)/2, -(frame.yMin + frame.yMax)/2);
}


void graphic::draw_base(Frame default_frame)
{
    cr->set_source_rgb(255., 255., 255.);
    cr->paint();
    cr->stroke();

    cr->set_source_rgb(0.66, 0.66, 0.66);
    cr->set_line_width(1);
    cr->move_to(default_frame.xMin, default_frame.yMin);
    cr->line_to(default_frame.xMin, default_frame.yMax);
    cr->line_to(default_frame.xMax, default_frame.yMax);
    cr->line_to(default_frame.xMax, default_frame.yMin);
    cr->line_to(default_frame.xMin, default_frame.yMin);
    cr->stroke();
}


void graphic::draw_circle(double centerx, double centery, double radius,
                            Colour color1, Colour color2)
{
    if(set_color(color2))
    {   
        cr->set_line_width(linewid);
        cr->arc(centerx, centery, radius, 0.0, 2.0 * cst::PI);
        cr->fill_preserve();
    }
    set_color(color1);
    cr->set_line_width(linewid);
    cr->arc(centerx, centery, radius, 0.0, 2.0 * cst::PI);
    cr->stroke();
}


void graphic::draw_square(double centerx, double centery, double side,
                            Colour color1, Colour color2)
{
    if(set_color(color2))
    {   
        cr->set_line_width(linewid);
        cr->move_to(centerx, centery+side/2.);
        cr->line_to(centerx+side/2., centery+side/2.);
        cr->line_to(centerx+side/2., centery-side/2.);
        cr->line_to(centerx-side/2., centery-side/2.);
        cr->line_to(centerx-side/2., centery+side/2.);
        cr->close_path();
        cr->fill_preserve();
    }
    
    set_color(color1);
    cr->set_line_width(linewid/2.);
    cr->move_to(centerx, centery+side/2.);
    cr->line_to(centerx+side/2., centery+side/2.);
    cr->line_to(centerx+side/2., centery-side/2.);
    cr->line_to(centerx-side/2., centery-side/2.);
    cr->line_to(centerx-side/2., centery+side/2.);
    cr->close_path();
    
    cr->stroke();
}


void graphic::draw_line(double cx1, double cy1, double cx2, double cy2, Colour color)
{
    set_color(color);
    cr-> move_to(cx1, cy1);
    cr->line_to(cx2, cy2);
    cr->stroke();
}