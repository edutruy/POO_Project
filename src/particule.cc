/**
 * @file particule.cc
 * @author J.Ropars 364156 (30%) and E.Dutruy 355726 (70%)
 * @date 2023-03-10
 * @version Rendu2
 * @brief src code particule module
 */

#include <iostream>
#include "particule.h"
#include "message.h"
#include "constantes.h"
#include "shape.h"


namespace
{
    void error_handler(std::string msg)
    {
        std::cout << msg;
    }

    #ifdef DEBUG
    /**
     * @brief this is a debug function
     */
    void print_particle_creation(shape::square shape_)
    {
        std::cout << "A particle was created : position (" 
            << shape_.center.x << "," << shape_.center.y << ") "
            << "size " << shape_.size << "\n";
    }
    #endif
}


// -------------------- public --------------------

Particule::Particule(double x, double y, double size)
{   
    shape::S2d position;
    position.x = x;
    position.y = y;
    shape_.center = position;
    shape_.size = size;

    #ifdef DEBUG
    print_particle_creation(shape_);
    #endif
}


shape::square Particule::get_shape() const
{
    return shape_;
}


bool Particule::is_ok(std::vector<Particule> particule_list, bool ezero_on) const
{
    if (shape_.size <= cst::d_particule_min) // control size
    {
        error_handler(message::particle_too_small(
                                                shape_.center.x, 
                                                shape_.center.y,
                                                shape_.size
                                                ));
        return false;
    }

    for (size_t i(0); i < particule_list.size(); ++i) // control superposition
    {
        if (shape::collision(shape_, particule_list[i].shape_, ezero_on))
        {
            error_handler(message::particle_superposition(
                                                shape_.center.x,
                                                shape_.center.y,
                                                particule_list[i].shape_.center.x,
                                                particule_list[i].shape_.center.y
                                                ));
            return false;
        }
    }

    if (not shape::is_inside(shape_, cst::dmax)) // check if it's inside domain
    {
        error_handler(message::particle_outside(
                                            shape_.center.x, 
                                            shape_.center.y, 
                                            shape_.size
                                            ));
        return false;
    }

    return true;
}


std::string Particule::info()
{
    std::string line("\t");
    line += std::to_string(shape_.center.x);
    line += " ";
    line += std::to_string(shape_.center.y);
    line += " ";
    line += std::to_string(shape_.size);
    line += "\n";

    return line;
}


void Particule::draw()
{
    shape::draw_square(shape_, ROUGE, GRIS);
}


bool Particule::large_enough() const
{
    if ( (shape_.size/2 - 2*shape::epsil_zero) 
        < (cst::d_particule_min + shape::epsil_zero) )
    {
        return false;
    }
    return true;
}


Particule Particule::sub_particule(int indicator) const
{
    double x(0);
    double y(0);
    double size(shape_.size/2 - 2*shape::epsil_zero);
    switch(indicator)
    {
    case 0: // top right
        x = shape_.center.x + shape_.size/4;
        y = shape_.center.y + shape_.size/4;
        break;

    case 1: // top left
        x = shape_.center.x - shape_.size/4;
        y = shape_.center.y + shape_.size/4;
        break;

    case 2: // bot right
        x = shape_.center.x + shape_.size/4;
        y = shape_.center.y - shape_.size/4;
        break;

    case 3: // bot left
        x = shape_.center.x - shape_.size/4;
        y = shape_.center.y - shape_.size/4;
        break;
    }

    Particule p(x, y, size);
    return p;
}
