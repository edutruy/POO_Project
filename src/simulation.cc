/**
 * @file simulation.cc
 * @author J.Ropars 364156 (10%) and E.Dutruy 355726 (90%)
 * @date 2023-03-07
 * @version Rendu2
 * @brief src code for simulation module
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <random>
#include "constantes.h"
#include "simulation.h"
#include "message.h"
#include "particule.h"
#include "robot.h"


namespace
{
    std::default_random_engine e; 

    void line_analysis(std::string line, std::vector<double> &data_vector)
    {
        if (line[0] == '#')
        {
            return;
        }
        
        std::istringstream data(line);
        std::string value;
        while (data >> value)
        {
            if (value[0] == '#')
            {
                break;
            }
            if (value == "true")
            {
                data_vector.push_back(1.0);
            }
            else if (value == "false")
            {
                data_vector.push_back(0.0);
            }
            else
            {
                double double_val = std::stod(value);
                data_vector.push_back(double_val);
            }
        }
    }

    #ifdef DEBUG
    /**
     * @brief this is a debug function
     */
    void print_raw_input(std::vector<double> &raw_data_input_)
    {
        std::cout << "The vector raw_data_input_ contains the following : \n";
        for (size_t i(0); i<raw_data_input_.size(); ++i)
        {
            std::cout << raw_data_input_[i] << " ";
        }
        std::cout << "\n";
    }
    #endif //DEBBUG

} // namespace


// -------------------- private --------------------

void Simulation::error_handler(std::string msg)
    {
        std::cout << msg;
        if (msg == message::success())
        {
            return;
        }
        simisok_ = false;
    }


void Simulation::get_data_from_file(std::ifstream& file)
{
    std::string line;
    while (std::getline(file >> std::ws, line))
    {
        line_analysis(line, raw_data_input_);
    }
    
    if (raw_data_input_.empty())
    {
        error_handler("File is empty");
    }
}


void Simulation::init_values(std::vector<double> &input)
{
    int vector_pos(0);
    // manage particules
    nbr_particules_ = input[vector_pos];
    for (int i(0); i < nbr_particules_; ++i)
    {
        Particule p(input[vector_pos+1], input[vector_pos+2], input[vector_pos+3]);
        simisok_ = p.is_ok(particule_list_, false);
        particule_list_.push_back(p); // important to check validity before push_back
        vector_pos += 3;
        if (not simisok_) { return; }
    }
    // manage robots
    simisok_ = space_robot_.init(input, vector_pos); // vector_pos is updated here
    if (not simisok_) { return; }
    simisok_ = not space_robot_.robot_particule_collision(particule_list_, false);
    if (not simisok_) { return; }
    // init all neutra/repa robots
    simisok_ = space_robot_.data_analysis(vector_pos, input, particule_list_, 
                            reparateur_list_, neutraliseur_list_);
    if (not simisok_) { return; }
    simisok_ = space_robot_.neutra_repa_ok(neutraliseur_list_, reparateur_list_);

    #ifdef DEBUG 
    std::cout << "Simulation is set up and ready \n";
    #endif
}


void Simulation::read_file(std::string filename)
{
    filename_ = filename;

    // check if we can open the file
    std::ifstream file(filename);
    if (file.fail())
    {
        error_handler(filename + " does not exist or does not open correctly");
    }

    if (simisok_)
    {
        get_data_from_file(file);
    }

    file.close();

    #ifdef DEBUG
    print_raw_input(raw_data_input_);
    #endif
}


void Simulation::reset_all_values()
{
    particule_list_.clear();
    reparateur_list_.clear();
    neutraliseur_list_.clear();
    nbr_particules_ = 0;
    space_robot_.reset_all_values();
}


std::string Simulation::get_file_content()
{
    std::string content("# Actual state of the simulation\n#\n");
    content += "# Nom du scenario de test: " + filename_ + "\n#\n";
    content += "# nombre de particules puis les donnees d une particule par ligne\n";

    content += "\n" + std::to_string(nbr_particules_) + "\n";
    for (size_t i(0); i < particule_list_.size(); ++i)
    {
        content += particule_list_[i].info();
    }
    
    content += "\n# donnees du robot spatial\n";
    content += space_robot_.info();

    content += "\n# donnees des nbRs robots reparateurs en service (un par ligne)\n";
    for (size_t i(0); i < reparateur_list_.size(); ++i)
    {
        content += reparateur_list_[i].info();
    }

    content += "\n";
    content += "# donnees des nbNs robots neutraliseurs en service (un par ligne)";
    content += "\n";
    for (size_t i(0); i < neutraliseur_list_.size(); ++i)
    {
        content += neutraliseur_list_[i].info();
    }
    
    return content;
}


void Simulation::desintegrate(int position)
{
    for (int i(0); i < 4; ++i)
    {
        Particule p = particule_list_[position].sub_particule(i);
        particule_list_.push_back(p); 
        ++nbr_particules_;
    }

    particule_list_.erase(particule_list_.begin() + position);
    --nbr_particules_;
}


int Simulation::get_nbNbreak() const
{
    int nbNbreak(0);
    for (size_t i(0); i < neutraliseur_list_.size(); ++i)
    {
        if (neutraliseur_list_[i].get_breakdown())
        {
            ++nbNbreak;
        }
    }
    return nbNbreak;
}


// -------------------- public --------------------

Simulation::Simulation(): 
    filename_(""), 
    raw_data_input_({}),
    nbr_particules_(0),
    particule_list_({}),
    reparateur_list_({}), 
    neutraliseur_list_({}),
    simisok_(true),
    good_to_go(false),
    proba_(cst::desintegration_rate)
{
    e.seed(1);

    #ifdef DEBUG
    std::cout << "new simulation object\n";
    #endif
}


Simulation::~Simulation()
{
    #ifdef DEBUG
    std::cout << "delete simulation object\n";
    #endif 
}


bool Simulation::init(std::string filename)
{
    read_file(filename);

    if (simisok_)
    {
        init_values(raw_data_input_);
    }

    if (simisok_)
    {
        error_handler(message::success());
    }

    if (not simisok_)
    {
        reset_all_values();
    }
    
    if (simisok_)
    {
        good_to_go = true;
    }
    return simisok_;
}


void Simulation::update()
{
    space_robot_.incr_nbUpdate();

    std::bernoulli_distribution b(proba_/nbr_particules_);
    for (int i(nbr_particules_-1); i >= 0; --i)
    {
        if (b(e) and particule_list_[i].large_enough())
        {
            desintegrate(i);
        }

    }
}


void Simulation::draw()
{
    shape::draw_base();

    if (not good_to_go)
    {
        return;
    }

    for (size_t i = 0; i < particule_list_.size(); ++i)
    {
        particule_list_[i].draw();
    }

    for (size_t i = 0; i < reparateur_list_.size(); ++i)
    {
        reparateur_list_[i].draw();
    }

    for (size_t i = 0; i < neutraliseur_list_.size(); ++i)
    {
        neutraliseur_list_[i].draw();
    }

    space_robot_.draw();
}


void Simulation::write_file()
{
    std::string content(get_file_content());

    std::ofstream file("project_output.txt");
    file << content;
    file.close();
    std::cout << "project_output.txt has been written\n";
}


std::string Simulation::sum_up_for_info_label()
{
	std::string content("Informations générales sur la simulation\n\n");
	content += "Mise à jour: \t\t\t\t\t\t\t" + 
				std::to_string(space_robot_.get_nbUpdate()) + "\n";
	content += "Particules: \t\t\t\t\t\t\t" + 
				std::to_string(nbr_particules_) + "\n";
	content += "Robots réparateurs en service: \t\t" + 
				std::to_string(space_robot_.get_nbRs()) + "\n";
	content += "Robots réparateurs en réserve: \t\t" + 
				std::to_string(space_robot_.get_nbRr()) + "\n";
	content += "Robots neutraliseurs en service: \t" + 
				std::to_string(space_robot_.get_nbNs()) + "\n";
	content += "Robots neutraliseurs en panne: \t\t" + 
				std::to_string(get_nbNbreak()) + "\n";
	content += "Robots neutraliseurs détruits: \t\t" + 
				std::to_string(space_robot_.get_nbNd()) + "\n";
	content += "Robots neutraliseurs en réserve: \t" + 
				std::to_string(space_robot_.get_nbNr()) + "\n";
	
	return content;
}