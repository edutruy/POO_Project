/**
 * @file gui.cc
 * @author J.Ropars 364156 (50%) and E.Dutruy 355726 (50%)
 * @date 2023-04-15
 * @version Rendu2
 * @brief Source code Graphic User Interface (GUI)
 */

#include <gtkmm.h>
#include <iostream>

#include "gui.h"
#include "constantes.h"
#include "graphic.h"
#include "simulation.h"


static Simulation* static_sim(nullptr);
static Frame default_frame = {-cst::dmax, cst::dmax, -cst::dmax, cst::dmax, 1.,
								cst::AREA_SIDE, cst::AREA_SIDE}; 


static void myprint(std::string msg)
{
	#ifdef DEBUG
	std::cout << msg << "\n";
	#endif
}


// --------------- DrawingArea private ----------------

void DrawingArea::adjust_frame(int width, int height)
{
	frame_.width  = width;
	frame_.height = height;

	// Preventing distorsion by adjusting the frame (cadrage)
	// to have the same proportion as the graphical area
	
    // use the reference framing as a guide for preventing distortion
    double new_aspect_ratio((double)width/height);
    if( new_aspect_ratio > default_frame.asp)
    { // keep yMax and yMin. Adjust xMax and xMin
	    frame_.yMax = default_frame.yMax ;
	    frame_.yMin = default_frame.yMin ;	
	  
	    double delta(default_frame.xMax - default_frame.xMin);
	    double mid((default_frame.xMax + default_frame.xMin)/2);
        // the new frame is centered on the mid-point along X
	    frame_.xMax = mid + 0.5*(new_aspect_ratio/default_frame.asp)*delta ;
	    frame_.xMin = mid - 0.5*(new_aspect_ratio/default_frame.asp)*delta ;		  	  
    }
    else
    { // keep xMax and xMin. Adjust yMax and yMin
	    frame_.xMax = default_frame.xMax ;
	    frame_.xMin = default_frame.xMin ;
	  	  
 	    double delta(default_frame.yMax - default_frame.yMin);
	    double mid((default_frame.yMax + default_frame.yMin)/2);
        // the new frame is centered on the mid-point along Y
	    frame_.yMax = mid + 0.5*(default_frame.asp/new_aspect_ratio)*delta ;
	    frame_.yMin = mid - 0.5*(default_frame.asp/new_aspect_ratio)*delta ;		  	  
    }
}


// --------------- DrawingArea protected ----------------

void DrawingArea::on_draw(const Cairo::RefPtr<Cairo::Context>& cr,
                      int width, int height)
{
	if (not empty_)   // drawing in the Model space
	{
		// adjust the frame (cadrage) to prevent distortion 
		adjust_frame(width, height);
	
		// on passe le pointeur cairo à graphic
		graphic::graphic_set_context(cr);
		// set the transformation MODELE to GTKmm
		graphic::orthographic_projection(frame_);
		(*static_sim).draw();
	}
	else
	{	
		graphic::graphic_set_context(cr);
		graphic::draw_base(frame_);
		myprint("empty !");
	}
}


// --------------- DrawingArea public ----------------

DrawingArea::DrawingArea() : empty_(false)
{
	set_content_width(cst::AREA_SIDE);
	set_content_height(cst::AREA_SIDE);

	set_draw_func(sigc::mem_fun(*this, &DrawingArea::on_draw));
}


DrawingArea::~DrawingArea()
{}


void DrawingArea::clear()
{
	empty_ = true; 
	queue_draw();
}


void DrawingArea::draw()
{
	empty_ = false;
	queue_draw();
}


// --------------- MyWindow private ----------------

void MyWindow::set_button_label()
{
	m_label_general.set_label("General");
    m_label_info.set_label(static_sim->sum_up_for_info_label());
    m_exit_button.set_label("exit");
    m_open_button.set_label("open");
    m_save_button.set_label("save");
    m_start_button.set_label("start");
    m_step_button.set_label("step");
}


void MyWindow::set_up_window()
{
	set_title("Propre en ordre");
	set_child(m_box_top);

	// set up top box
	m_box_top.append(m_left_box);
	m_box_top.append(m_right_box);

	// set up left box
	m_left_box.append(m_label_general);
	m_left_box.append(m_box_buttons);
	m_left_box.append(m_separator1);
	m_left_box.append(m_label_info);
	m_left_box.append(m_box_info);

	// set up right box
	m_right_box.append(m_drawing_area);
	m_drawing_area.set_expand();

	// Set up m_box_buttons
	m_box_buttons.append(m_exit_button);
	m_box_buttons.append(m_open_button);
	m_box_buttons.append(m_save_button);
	m_box_buttons.append(m_start_button);
	m_box_buttons.append(m_step_button);
}


void MyWindow::connect_buttons()
{
	m_exit_button.signal_clicked().connect(sigc::mem_fun(*this,
              &MyWindow::exit_button_clicked));
	m_open_button.signal_clicked().connect(sigc::mem_fun(*this,
              &MyWindow::open_button_clicked));
	m_save_button.signal_clicked().connect(sigc::mem_fun(*this,
              &MyWindow::save_button_clicked));
	m_start_button.signal_clicked().connect(sigc::mem_fun(*this,
              &MyWindow::start_button_clicked));
	m_step_button.signal_clicked().connect(sigc::mem_fun(*this,
              &MyWindow::step_button_clicked));
}


void MyWindow::connect_keyboard()
{
    auto controller = Gtk::EventControllerKey::create();
    controller->signal_key_pressed().connect(
                  sigc::mem_fun(*this, &MyWindow::on_key_pressed), false);
    add_controller(controller);
}


void MyWindow::set_up_timer()
{
	timer_added_ = false;
	disconnect_ = false;
	timer_val_ = 1;
}


void MyWindow::restart(std::string filename)
{
	delete static_sim;
    static_sim = new Simulation;
    static_sim->init(filename);
}


void MyWindow::action_start()
{
	if (not timer_added_)
	{
		m_start_button.set_label("stop");
		sigc::slot<bool()> my_slot = sigc::bind(sigc::mem_fun(*this,
		                                        &MyWindow::on_timeout));
		auto conn = Glib::signal_timeout().connect(my_slot, cst::delta_t*1000);
			
		timer_added_ = true;
		myprint("Start button was clicked -> Timer on");

		return;
	}

	m_start_button.set_label("start");
	myprint("Stop button was clicked -> Timer off");
	disconnect_  = true;
	timer_added_ = false;
}


void MyWindow::action_step()
{
	static_sim->update();
	m_label_info.set_text(static_sim->sum_up_for_info_label());

	++timer_val_;
	myprint("Step button was clicked");
}


void MyWindow::on_dialog_response(int response_id, Gtk::FileChooserDialog* dialog)
{
	//Handle the response:
	switch (response_id)
	{
		case Gtk::ResponseType::OK:
		{
		    //Notice that this is a std::string, not a Glib::ustring.
		    auto filename = dialog->get_file()->get_path();
		    myprint("Open clicked.");
		    myprint("File selected: " + filename);
			restart(filename);
			m_label_info.set_text(static_sim->sum_up_for_info_label());
			m_drawing_area.draw();
		    break;
		}
		case Gtk::ResponseType::CANCEL:
		{
		    myprint("Cancel clicked.");
		    break;
		}
		default:
		{
		    myprint("Unexpected button clicked.");
		    break;
		}
	}
	delete dialog;
}


bool MyWindow::on_key_pressed(guint keyval, guint keycode, Gdk::ModifierType state)
{
	switch(gdk_keyval_to_unicode(keyval))
	{
		case 's':
			action_start();
			return true;
		
		case '1':
			action_step();
			return true;

	}
	return false;
}


bool MyWindow::on_timeout()
{	
	if(disconnect_)
	{
		disconnect_ = false; // reset for next time a Timer is created
		
		return false; // End of Timer 
	}
	
	static_sim->update();
	m_label_info.set_text(static_sim->sum_up_for_info_label());
	m_drawing_area.draw();

	++timer_val_;
	return true; 
}


// --------------- MyWindow protected ----------------

void MyWindow::exit_button_clicked()
{
	myprint("Exit button was clicked");
	hide();
}


void MyWindow::open_button_clicked()
{
	myprint("Open button was clicked");
	
	auto dialog = new Gtk::FileChooserDialog("Please choose a file",
		  Gtk::FileChooser::Action::OPEN);
		  
	dialog->set_transient_for(*this);
	dialog->set_modal(true);
	dialog->signal_response().connect(sigc::bind(
	sigc::mem_fun(*this, &MyWindow::on_dialog_response), dialog));
	
	// Add response buttons to the dialog:
	dialog->add_button("_Cancel", Gtk::ResponseType::CANCEL);
	dialog->add_button("_Open", Gtk::ResponseType::OK);
	
	// Add filters, so that only certain file types can be selected:
	auto filter_text = Gtk::FileFilter::create();
	filter_text->set_name("Text files");
	filter_text->add_mime_type("text/plain");
	dialog->add_filter(filter_text);
	
	auto filter_cpp = Gtk::FileFilter::create();
	filter_cpp->set_name("C/C++ files");
	filter_cpp->add_mime_type("text/x-c");
	filter_cpp->add_mime_type("text/x-c++");
	filter_cpp->add_mime_type("text/x-c-header");
	dialog->add_filter(filter_cpp);
	
	auto filter_any = Gtk::FileFilter::create();
	filter_any->set_name("Any files");
	filter_any->add_pattern("*");
	dialog->add_filter(filter_any);

	// Show the dialog and wait for a user response:
	dialog->show();
}


void MyWindow::save_button_clicked()
{
	static_sim->write_file();
    myprint("Save button was clicked - Simulation has been reported in a file");
}


void MyWindow::start_button_clicked()
{
	action_start();
}


void MyWindow::step_button_clicked()
{
	action_step();
}


// --------------- MyWindow public ----------------

MyWindow::MyWindow(Simulation* simptr) :
	m_box_top(Gtk::Orientation::HORIZONTAL, 10),
	m_left_box(Gtk::Orientation::VERTICAL, 10),
	m_right_box(Gtk::Orientation::VERTICAL, 10), 
	m_box_buttons(Gtk::Orientation::VERTICAL, 10), 
	m_box_info(Gtk::Orientation::VERTICAL, 10)
{
	static_sim = simptr;

	set_up_timer();
    set_button_label();
	set_up_window();
	connect_buttons();
	connect_keyboard();
}


MyWindow::~MyWindow()
{
	delete static_sim;
}