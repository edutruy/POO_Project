/**
 * @file robot.h
 * @author J.Ropars 364156 and E.Dutruy 355726
 * @date 2023-03-12
 * @version Rendu2
 * @brief robot module interface
 */

#ifndef ROBOT_H
#define ROBOT_H

#include <vector>
#include "shape.h"
#include "particule.h"


// -------------------- Robot top class --------------------

class Robot
{
protected:
    shape::circle shape_;

public:
    virtual ~Robot();
    
    shape::circle get_circle() const;

    /**
     * @return true if there is a collision between a robot and a particule
     */
    bool robot_particule_collision(std::vector<Particule> p_vec, bool ezero_on);

    /**
     * @brief fonction de dessin, redéfinie dans les sous-classes
     * 
     */
    virtual void draw() = 0;

    /**
     * @brief get info on the robot, redefined in the subclasses
     * 
     * @return string with infos
     */
    virtual std::string info() = 0;
};

// -------------------- Neutraliseur --------------------

class Neutraliseur: public Robot
{
private:
    double orientation_;
    int coordination_type_;
    bool breakdown_;
    int k_update_breakdown_;
    bool in_contact_;

public:
    Neutraliseur(std::vector<double> &input, int &vector_pos);
    virtual ~Neutraliseur();

    bool get_breakdown() const;
    int get_k_update_breakdown() const;

    /**
     * @brief fait en sorte que l'orientation reste entre pi et -pi
     * 
     * @param orientation 
     */
    void set_orientation(double orientation);

    /**
     * @brief check si 2 neutraliseurs se chevauchent
     * 
     * @param neutraliseur_list 
     * @return true if it is ok
     * @return false if it is not ok
     */
    bool is_ok(std::vector<Neutraliseur> neutraliseur_list) const;
    
    /**
     * @brief check si k_update_breakdown est inférieur au nombre d'update
     * 
     * @param nbUpdate 
     * @return true si k_update_breakdown est inférieur
     */
    bool check_k_update_breakdown(int nbUpdate) const;

    /**
     * @brief get all the relevant informations in the require format for the file
     */
    virtual std::string info() override;

    /**
     * @brief draw le neutraliseur
     */
    virtual void draw() override;
};


// -------------------- Reparateur --------------------

class Reparateur: public Robot
{
private:
    shape::S2d destination_;

public:
    Reparateur(double x, double y);
    virtual ~Reparateur();

    /**
     * @brief check si deux réparateur se chevauchent
     * 
     * @param particule_list std::vector<Paricule>
     * @return true/false
     */
    bool is_ok(std::vector<Reparateur> reparateur_list) const;

    /**
     * @brief get all the relevant informations in the require formate for the file
     */
    virtual std::string info() override;

    /**
     * @brief draw le reparateur
     */
    virtual void draw() override;
};


// -------------------- Spatial --------------------

class Spatial: public Robot
{
private:
    int nbUpdate_;
    int nbNr_; // nombre de neutraliseur en reserve
    int nbNs_; // nombre de neutraliseur en service
    int nbNd_; // nombre de neutraliseur detruit
    int nbRr_; // nombre de reparateur en reserve
    int nbRs_; // nombre de reparateur en service

public:
    Spatial();
    virtual ~Spatial();

    /**
     * @return the total number of neutraliseur (nbNr + nbNs + nbNd)
     */
    int get_nbr_neutraliseur() const;

    /** 
     * @return the total number of reparateur (nbRr + nbRs)
     */
    int get_nbr_reparateur() const;
    int get_nbNs() const;
    int get_nbRs() const;
    int get_nbRr() const;
    int get_nbNd() const;
    int get_nbNr() const;
    int get_nbUpdate() const;
    void incr_nbUpdate();

    void reset_all_values();

    /**
     * @brief pass the value from a raw data vector to the attributes
     * 
     * @param input vector<double> such as raw_data_input_
     * @param vector_pos position to start -1
     */
    bool init(std::vector<double> &input, int &vector_pos);

    /**
     * @brief check superposition between neutraliseur and reparateur robots
     * 
     * @param n_ls 
     * @param r_ls 
     * @return true if there is no superposition between them
     */
    bool neutra_repa_ok(std::vector<Neutraliseur> neutra_list, 
                        std::vector<Reparateur> repa_list) const;

    /**
     * @brief get data from the input vector and create all robots 
     * 
     * @param vector_pos int
     * @param input vector<double>
     * @param particule_list vector<Particule>
     * @param reparateur_list vector<Reparateur>
     * @param neutraliseur_list vector<Neutraliseur>
     * @return true if everything went well
     */
    bool data_analysis(int& vector_pos, std::vector<double> input, 
                        std::vector<Particule> &particule_list,
                        std::vector<Reparateur> &reparateur_list,
                        std::vector<Neutraliseur> &neutraliseur_list);
    
    /**
     * @brief get all the relevant informations in the require formate for the file
     */
    virtual std::string info() override;

    /**
     * @brief draw une ligne
     */
    virtual void draw() override;
};

#endif /* ROBOT_H */
