/**
 * @file graphics.h
 * @author J.Ropars 364156 and E.Dutruy 355726
 * @date 2023-03-24
 * @version Rendu2
 * @brief Pour le GUI avec GTKmm4 (interface)
 */
#ifndef GRAPHIC_H
#define GRAPHIC_H

#include <gtkmm.h>
#include <iostream>
#include <cairomm/context.h>


enum Colour{NONE, ROUGE, GRIS, BLEU, VERT, VIOLET, ORANGE, BLACK, BLANC};


struct Frame // Model Framing and window parameters
{
	double xMin; // frame parameters
	double xMax;
	double yMin;
	double yMax;
	double asp;  // frame aspect ratio
	int height;  // window height
	int width;   // window width
};


namespace graphic
{
	/**
	 * @brief créer un pointeur sur cr, pour y avoir accès dans graphic
	 * 
	 * @param cr
	 */
	void graphic_set_context(const Cairo::RefPtr<Cairo::Context>& ptcrr);

	/**
	 * @brief adapt gtkmm coordinate to model coordinate
	 * 
	 * @param frame 
	 */
	void orthographic_projection(Frame frame);

	/**
	 * @brief dessine la bordure exterieur du domaine et le fond blanc
	 * 
	 * @param default_frame 
	 */
	void draw_base(Frame default_frame);

	/**
	 * @brief dessine un cercle, color1 => cadre, color2 => fond
	 * 
	 * @param centerx 
	 * @param centery 
	 * @param radius 
	 * @param color1 
	 * @param color2 
	 */
	void draw_circle(double centerx, double centery, double radius,
					Colour color1, Colour color2);

	/**
	 * @brief dessine un carré, color1 => cadre, color2 => fond
	 * 
	 * @param centerx 
	 * @param centery 
	 * @param side 
	 * @param color1 
	 * @param color2 
	 */
	void draw_square(double centerx, double centery, double side,
					Colour color1, Colour color2);

	/**
	 * @brief dessine une ligne 
	 * 
	 * @param cx1 
	 * @param cy1 
	 * @param cx2 
	 * @param cy2 
	 * @param color 
	 */
	void draw_line(double cx1, double cy1, double cx2, double cy2, Colour color);
}


#endif /* GRAPHICS_H */